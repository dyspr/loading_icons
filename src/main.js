var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.1
var circleSize = 0.3333
var resolution = 100
var icons = []
var dimension = 4
for (var i = 0; i < dimension; i++) {
  for (var j = 0; j < dimension; j++) {
    icons.push([i, j])
  }
}
// TODO: change frameCount to deltaTime

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < dimension; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5) + 0.5) * boardSize * initSize * 2, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5) + 0.5) * boardSize * initSize * 2)
      drawIcon(initSize * 0.8333, (i + j) % 2, (frameCount * 0.1) / (Math.PI * 2), 255)
      pop()

      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5) + 0.5) * boardSize * initSize * 2, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5) + 0.5) * boardSize * initSize * 2)
      drawIcon(initSize * 0.5, (i + j + 1) % 2, (frameCount * 0.1) / (Math.PI * 2) + Math.PI * 0, 255)
      pop()

      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5) + 0.5) * boardSize * initSize * 2, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5) + 0.5) * boardSize * initSize * 2)
      fill(255 * ((i + j) % 2))
      noStroke()
      ellipse(0, 0, boardSize * initSize * 0.6666)
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawIcon(size, type, state, col) {
  var direction = null
  if (type === 0) {
    direction = 1
  } else {
    direction = -1
  }
  rotate(state * direction)
  for (var i = 0; i < resolution; i++) {
    fill(col * (i / resolution))
    noStroke()
    push()
    translate(boardSize * size * (-direction) * sin(Math.PI * 2 * (i / resolution)), boardSize * size * cos(Math.PI * 2 * (i / resolution)))
    ellipse(0, 0, boardSize * initSize * circleSize)
    pop()
  }
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
